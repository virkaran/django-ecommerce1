from django import forms  
from django.forms import ModelForm
from django.db import models
# from django.forms import modelformset_factory
from car_rent.models import CarsModel, CustomersModel,StoresModel




class CarsForm(forms.Form):
    make = forms.CharField(max_length=50, blank=True, null=True)
    model = forms.CharField(max_length=50, blank=True, null=True)
    modelyear = forms.IntegerField( blank=True, null=True)
    pricenew = forms.IntegerField(blank=True, null=True)
    fueltype = forms.CharField( max_length=50, blank=True, null=True) 
    tankcapacity = forms.CharField( max_length=8, blank=True, null=True) 
    power = forms.CharField(max_length=8, blank=True, null=True)
    seatingcapacity = forms.IntegerField( blank=True, null=True)  


class CustomerForm(ModelForm):
    class Meta:
        model = CustomersModel
        fields=['customername','dob','address','occupation','phone','gender']

class StoresForm(ModelForm):
    class Meta:
        model =StoresModel
        fields = '__all__'
