from django.urls import path

from car_rent import views

urlpatterns = [
    path('', views.cars_detail, name='cars'),
    ]