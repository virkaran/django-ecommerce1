from django.contrib import admin
from django.db import models
from car_rent.models import CarsModel,CustomersModel,StoresModel,AdminUserModel,OrderModel

# Register your models here.
admin.site.register(CarsModel)
admin.site.register(CustomersModel)
admin.site.register(StoresModel)
admin.site.register(AdminUserModel)
admin.site.register(OrderModel)