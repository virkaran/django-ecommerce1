
from django.http import HttpResponseRedirect
from django.shortcuts import render

from .forms import CarsForm,CustomerForm

def cars_detail(request):
    if request.method == 'POST':
        form = CarsForm(request.POST)
        if form.is_valid():
            return HttpResponseRedirect('/thanks/')
   
    else:
        form = CarsForm()

    return render(request, 'cars.html', {'form': form})

def customers_detail(request):
    if request.method == 'POST':
        form = CustomerForm(request.POST)
        if form.is_valid():
            return HttpResponseRedirect('/thanks/')
   
    else:
        form =CustomerForm()

    return render(request, 'customers.html', {'form': form})