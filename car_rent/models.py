from django.db import models

# Create your models here.
class CarsModel(models.Model):
    make = models.CharField(max_length=50, blank=True, null=True)
    model = models.CharField(max_length=50, blank=True, null=True)
    modelyear = models.IntegerField( blank=True, null=True)
    pricenew = models.IntegerField(blank=True, null=True)
    fueltype = models.CharField( max_length=50, blank=True, null=True) 
    tankcapacity = models.CharField( max_length=8, blank=True, null=True) 
    power = models.CharField(max_length=8, blank=True, null=True)
    seatingcapacity = models.IntegerField( blank=True, null=True)  

class CustomersModel(models.Model):

    customername = models.CharField( max_length=50)  
    dob = models.DateField()
    address = models.CharField(max_length=75)
    occupation = models.CharField(max_length=50)
    phone = models.CharField(max_length=50)
    gender = models.CharField(max_length=1)

    @property
    def _get_name(self):
            return self.customername
    
    def _set_name(self, value):
            self.customername= value
    
    def __str__(self):
        return self.customername




class StoresModel(models.Model):

    storename = models.CharField(max_length=50, blank=True, null=True)
    storeaddress = models.CharField( max_length=50, blank=True, null=True)  
    phoneno = models.CharField( max_length=50, blank=True, null=True)  
    city = models.CharField(max_length=50, blank=True, null=True)
    state = models.CharField(max_length=50, blank=True, null=True)


    @property
    def _get_name(self):
            return self.storename
    
    def _set_name(self, value):
            self.storename = value
    def __str__(self):
        return self.storename



class AdminUserModel(models.Model):
    password = models.CharField(max_length=128)
    superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)


    @property
    def _get_name(self):
            return self.superuser
    
    def _set_name(self, value):
            self.superuser=value
    def __str__(self):
        return self.superuser


class OrderModel(models.Model):
    user = models.ForeignKey(CustomersModel,on_delete=models.CASCADE)
    car = models.ForeignKey(CarsModel,on_delete=models.CASCADE)
    start_date = models.DateTimeField(auto_now_add=True)
    end_date = models.CharField(max_length=20)
    approved = models.BooleanField(default=False)
    finished = models.BooleanField(default=False)
    canceled = models.BooleanField(default=False)
    paid = models.BooleanField(default=False)
    total_price = models.FloatField(null=True, blank=True)
    rate = models.IntegerField(default=0)
   
    @property
    def _get_name(self):
            return self.user
    
    def _set_name(self, value):
            self.user = value
    def __str__(self):
        return self.user
